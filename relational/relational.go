package relational

import (
	"context"
	"database/sql"
	_ "github.com/denisenkom/go-mssqldb"
	_ "github.com/go-sql-driver/mysql"
	_ "github.com/jackc/pgx/v5"
	// тянет кучу лишних либ
	//_ "github.com/godror/godror"
	_ "github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/pkg/errors"
	"gitlab.com/golang-libs/databases.git/config"
	"log"
)

var db *sqlx.DB

type DBXExecutorFunc func(tx *sqlx.Tx) (interface{}, error)
type DBExecutorFunc func(db *sqlx.DB) (interface{}, error)

// Для создания, например, логовой БД
func EstablishNewDB(dbconf config.DBConfig) (db *sqlx.DB, err error) {
	connStr := dbconf.GetConnectionStr()
	db, err = sqlx.Connect(dbconf.DriverName, connStr)
	if err != nil {
		return db, err
	}

	_, err = db.Exec(dbconf.GetTimezoneStmt())
	if err != nil {
		return db, err
	}
	// Пингуется он саммим sqlx.Connect
	db.SetMaxIdleConns(dbconf.IdleConns)
	db.SetMaxOpenConns(dbconf.OpenConns)

	return db, err
}

func EstablishDefaultDB(dbconf config.DBConfig) (err error) {
	db, err = EstablishNewDB(dbconf)
	return
}

func initTrnXOnDB(extTx *sqlx.Tx, extDB *sqlx.DB, initializer TxInitializer) (tx *sqlx.Tx, err error, externalTrn bool) {
	externalTrn = extTx != nil
	if externalTrn {
		return extTx, nil, externalTrn
	} else {
		tx, err = initializer(extDB)
		return
	}
}

type TxInitializer func(extDB *sqlx.DB) (tx *sqlx.Tx, err error)

func ROTx(extDB *sqlx.DB) (tx *sqlx.Tx, err error) {
	return extDB.BeginTxx(context.Background(), &sql.TxOptions{ReadOnly: true})
}

func RWTx(extDB *sqlx.DB) (tx *sqlx.Tx, err error) {
	return extDB.BeginTxx(context.Background(), &sql.TxOptions{ReadOnly: false})
}

func doX(f DBXExecutorFunc, extTx *sqlx.Tx, db *sqlx.DB, txInitializer TxInitializer) (intf interface{}, err error) {
	// Инициализируем новую транзакцию, если мы не в существующей
	tx, err, extTrn := initTrnXOnDB(extTx, db, txInitializer)
	if err != nil {
		return nil, err
	}

	defer func() {
		if r := recover(); r != nil {
			if !extTrn {
				err2 := tx.Rollback()
				if err2 != nil {
					log.Println("Cannot rollback transaction:", err2)
				}
			}
			switch x := r.(type) {
			case string:
				err = errors.New(x)
			case error:
				err = x
			default:
				err = errors.New("unknown panic")
			}
		}
	}()

	intf, err = f(tx)

	// Откат при ошибке
	if err != nil {
		if !extTrn {
			err2 := tx.Rollback()
			if err2 != nil {
				log.Println("Cannot rollback transaction:", err2)
			}
		}
		return intf, err
	}

	// Коммит, если все норм и мы начинали транзакцию
	if !extTrn {
		err = tx.Commit()
	}
	return
}

// DoX - функция для выполнения запросов к БД в транзакции
// f - функция, которая принимает транзакцию и возвращает результат
// extTx - внешняя транзакция, если она есть
// Возвращает результат выполнения функции и ошибку
func DoX(f DBXExecutorFunc, extTx *sqlx.Tx) (intf interface{}, err error) {
	return doX(f, extTx, db, RWTx)
}

// DoXRo - функция для выполнения запросов к БД в транзакции ТОЛЬКО НА ЧТЕНИЕ
// f - функция, которая принимает транзакцию и возвращает результат
// extTx - внешняя транзакция, если она есть
// Возвращает результат выполнения функции и ошибку
func DoXRo(f DBXExecutorFunc, extTx *sqlx.Tx) (intf interface{}, err error) {
	return doX(f, extTx, db, ROTx)
}

// DoXOnDB - функция для выполнения запросов к БД в транзакции
// f - функция, которая принимает транзакцию и возвращает результат
// extTx - внешняя транзакция, если она есть
// thatDB - база данных, на которой нужно выполнить запрос
// Возвращает результат выполнения функции и ошибку
func DoXOnDB(f DBXExecutorFunc, extTx *sqlx.Tx, thatDB *sqlx.DB) (intf interface{}, err error) {
	return doX(f, extTx, thatDB, RWTx)
}

// DoXRo - функция для выполнения запросов к БД в транзакции ТОЛЬКО НА ЧТЕНИЕ
// f - функция, которая принимает транзакцию и возвращает результат
// extTx - внешняя транзакция, если она есть
// thatDB - база данных, на которой нужно выполнить запрос
// Возвращает результат выполнения функции и ошибку
func DoXRoOnDB(f DBXExecutorFunc, extTx *sqlx.Tx, thatDB *sqlx.DB) (intf interface{}, err error) {
	return doX(f, extTx, thatDB, ROTx)
}

// Функция для выполнения запросов к БД без транзакции
func Do(f DBExecutorFunc) (intf interface{}, err error) {
	// Создаем копию db
	dbCopy := db
	return f(dbCopy)
}

// deprecated: use DoX
func NewTransaction() (*sqlx.Tx, error) {
	return db.Beginx()
}

func Close() {
	_ = db.Close()
}

func Ping() error {
	return db.Ping()
}

func Stats() sql.DBStats {
	return db.Stats()
}
