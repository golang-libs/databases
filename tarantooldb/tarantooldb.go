package tarantooldb

import (
	"context"
	"fmt"
	"github.com/pkg/errors"
	"github.com/tarantool/go-tarantool/v2"
	"github.com/tarantool/go-tarantool/v2/pool"
	"github.com/vmihailenco/msgpack/v5"
	"gitlab.com/golang-libs/databases.git/config"
	"log"
	"time"
)

// single used for conn.GetTyped for decode one tuple.
type Single struct {
	Res   interface{}
	found bool
}

func (s *Single) DecodeMsgpack(d *msgpack.Decoder) error {
	var err error
	var len int
	if len, err = d.DecodeArrayLen(); err != nil {
		return err
	}
	if s.found = len >= 1; !s.found {
		return nil
	}
	if len != 1 {
		return errors.New("tarantool returns unexpected value for Select(limit=1)")
	}
	return d.Decode(s.Res)
}

func Count(space, index string, values []interface{}) (int64, error) {
	req := tarantool.NewCallRequest(fmt.Sprintf("box.space.%s.index.%s:count", space, index))

	r, err := Client.Do(req, pool.PreferRO).Get()
	if err != nil {
		return 0, err
	}
	// Уродливо, да
	//data := r[0].([]interface{})
	switch val := r[0].(type) {
	case uint64:
		return int64(val), nil
	case uint32:
		return int64(val), nil
	case uint16:
		return int64(val), nil
	case uint8:
		return int64(val), nil
	default:
		return 0, errors.New("Ошибка получения количества записей для " + space + ":" + index)
	}
}

func CountLegacy(space, index string, values []interface{}) (int64, error) {
	data, err := Client.Call(fmt.Sprintf("box.space.%s.index.%s:count", space, index), values, pool.ANY)
	if err != nil {
		return 0, err
	}

	switch val := data[0].(type) {
	case uint64:
		return int64(val), nil
	case uint32:
		return int64(val), nil
	case uint16:
		return int64(val), nil
	case uint8:
		return int64(val), nil
	default:
		return 0, errors.New("Ошибка получения количества записей для " + space + ":" + index)
	}
}

var Client *pool.ConnectionPool

func EstablishNewDB(dbconf config.DBConfig) (*pool.ConnectionPool, error) {
	instances := make([]pool.Instance, len(dbconf.Hosts), len(dbconf.Hosts))
	for i, host := range dbconf.Hosts {
		instances[i] = pool.Instance{
			Name: fmt.Sprintf("host-%d(%s)", i, host),
			Dialer: tarantool.NetDialer{
				Address:  host,
				User:     dbconf.User,
				Password: dbconf.Pass,
			},
		}
	}

	ctx, _ := context.WithTimeout(context.Background(), time.Duration(30)*time.Second)
	conn, err := pool.Connect(ctx, instances)
	if err != nil {
		log.Fatalf("Failed to connect: %s", err.Error())
	}

	pt := tarantool.NewPingRequest()

	_, err = conn.Do(pt, pool.ANY).Get()
	if err != nil {
		log.Fatalf("Failed to ping: %s", err.Error())
	}

	return conn, nil
}

func EstablishDefaultDB(dbconf config.DBConfig) (err error) {
	Client, err = EstablishNewDB(dbconf)
	return
}

func Close() {
	_ = Client.Close()
}
