package redis

import (
	"context"
	"github.com/redis/go-redis/v9"
	"go.uber.org/zap"
)

type loggerWriter struct{}

func (loggerWriter) Printf(ctx context.Context, format string, v ...interface{}) {
	zap.S().Infof(format, v...)
}

func initRedisLog() {
	var lw = loggerWriter{}
	redis.SetLogger(lw)
}
