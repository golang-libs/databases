package elastic

import (
	"fmt"
	"github.com/elastic/go-elasticsearch/v7"
	"github.com/elastic/go-elasticsearch/v7/esapi"
	"gitlab.com/golang-libs/databases.git/config"
	"net/http"
	"time"
)

var MainCli *elasticsearch.Client

func EstablishNewDB(dbconf config.DBConfig) (*elasticsearch.Client, error) {
	//f := (&net.Dialer{Timeout: time.Second}).DialContext

	addrs := dbconf.Hosts
	if len(addrs) == 0 {
		addrs = []string{fmt.Sprintf("%s:%s", dbconf.Host, dbconf.Port)}
	}

	cfg := elasticsearch.Config{
		Addresses: addrs,
		Username:  dbconf.User,
		Password:  dbconf.Pass,
		Transport: &http.Transport{
			MaxIdleConnsPerHost:   dbconf.IdleConns,
			ResponseHeaderTimeout: time.Second * 3,
			//DialContext:           f,
			//TLSClientConfig: &tls.Config{
			//	MinVersion: tls.VersionTLS11,
			//	// ...
			//},
		},
	}
	es, err := elasticsearch.NewClient(cfg)
	if err != nil {
		return nil, err
	}

	_, err = es.Ping(func(request *esapi.PingRequest) {})
	if err != nil {
		return es, err
	}

	return es, nil
}

func EstablishDefaultDB(dbconf config.DBConfig) (err error) {
	MainCli, err = EstablishNewDB(dbconf)
	return
}
